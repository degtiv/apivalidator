package space.deg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Automation tests to check status and response using Java and java.net.* libraries.
 * There is a simple HTTP Request & Response Service https://httpbin.org
 *
 *   GET/delay/{delay}
 *   Returns a delayed response (max of 10 seconds);
 *
 *   GET/image/png
 *   Returns a simple PNG image
 *
 * Created by Degtyarev Ilya on 27.02.2019.
 * degtiv@yandex.ru
 */

public class DelayAndPngImageChecker {

    private final String USER_AGENT = "Mozilla/5.0";

    private class StatusAndResponse {
        StatusAndResponse() {
            status = 0;
            response = "";
            headers = new HashMap<>();
        }

        int status;
        String response;
        Map<String, List<String>> headers;
    }
    
    public void act(){
        System.out.println("Checking status and response 'http://httpbin.org':\n");
        DelayAndPngImageChecker checker = new DelayAndPngImageChecker();

        System.out.println("1) 'GET/delay/{delay}': ");
        if (checker.checkDelay())
            Log.printSuccess();
        else
            Log.printFailure();

        System.out.print("2) 'GET/image/png': ");
        if (checker.checkPngImage())
            Log.printSuccess();
        else
            Log.printFailure();

    }

    private boolean checkDelay() {
        boolean result = true;
        String request = "http://httpbin.org/delay/{delay}";

        for (int i = 0; i <= 10; i++) {
            StatusAndResponse statusAndResponse = new StatusAndResponse();

            String thisDelayRequest = request.replace("{delay}", String.valueOf(i));
            System.out.println(thisDelayRequest);
            try {
                statusAndResponse = getResponse(thisDelayRequest, "application/json");
            } catch (Exception e) {
                System.out.println(e.toString());
                result = false;
            }
            if (statusAndResponse.status != 200) {
                System.out.println("Status code is not 200");
                result = false;
            }

        }
        return result;
    }

    private boolean checkPngImage() {
        boolean result = true;
        StatusAndResponse statusAndResponse = new StatusAndResponse();
        String request = "http://httpbin.org/image/png";
        System.out.println(request);
        try {
            statusAndResponse = getResponse(request, "image/png");
        } catch (Exception e) {
            System.out.println(e.toString());
            result = false;
        }
        if (statusAndResponse.status != 200) {
            System.out.println("Status code is not 200");
            result = false;
        }
        if (!statusAndResponse.headers.get("Content-Type").get(0).equalsIgnoreCase("image/png")) {
            System.out.println("Response Content-Type is not 'image/png'");
            result = false;
        }
        return result;
    }

    private StatusAndResponse getResponse(String url, String contentType) throws Exception{
        try {
            StatusAndResponse statusAndResponse = new StatusAndResponse();
            URL urlObject = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
            connection.setRequestProperty("Content-Type", contentType);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", USER_AGENT);


            statusAndResponse.status = connection.getResponseCode();
            statusAndResponse.headers = connection.getHeaderFields();

            System.out.println("Resoponse Code: " + statusAndResponse.status);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = bufferedReader.readLine()) != null)
                response.append(inputLine);

            bufferedReader.close();

            statusAndResponse.response = response.toString();
            return statusAndResponse;
        } catch (IOException ioe) {
            throw new Exception();
        }
    }
}

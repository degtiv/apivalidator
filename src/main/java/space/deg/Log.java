package space.deg;

public class Log {
    private static final String SET_TEXT_RED = "\u001B[31m";
    private static final String SET_TEXT_GREEN = "\u001B[32m";
    private static final String SET_TEXT_DEFAULT = "\u001B[0m";

    public static void printSuccess() {
        System.out.println(SET_TEXT_GREEN);
        System.out.println("Result: Success;");
        System.out.println(SET_TEXT_DEFAULT);
    }
    public static void printFailure() {
        System.out.println(SET_TEXT_RED);
        System.out.println("Result: Failure;");
        System.out.println(SET_TEXT_DEFAULT);
    }
    public static void printException(String exceptionText) {
        System.out.println(SET_TEXT_RED);
        System.out.println(exceptionText);
        System.out.println("Result: Failure;");
        System.out.println(SET_TEXT_DEFAULT);
    }
}

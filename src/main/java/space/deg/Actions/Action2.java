package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.interactions.Mouse;
import space.deg.Action;

import java.util.HashMap;
import java.util.List;

/**
 * Action 2:
 * Check items under Who We Serve for sub-header
 *   - There are 11 items under resource sub-header
 *   - Titles are "Students", "Instructors", "Book Authors",
 *     "Professionals", "Researchers", "Institutions", "Libraries", "Corporations",
 *     "Societies", "Journal Editors", "Goverment"
 *
 * Created by Degtyarev Ilya on 27.02.2019.
 * degtiv@yandex.ru
 */

public class Action2 implements Action {
    /**
     * subHeaders compare a title ("Students", "Instructors", ...) and boolean that means is this title displayed
     */
    private HashMap<String, Boolean> subHeaders = new HashMap<>();

    private boolean isTheFirstPointTrue = false;
    private boolean isTheSecondPointTrue = false;


    @Override
    public boolean act(WebDriver driver) throws Exception {
        subHeaders.put("Students", false);
        subHeaders.put("Instructors", false);
        subHeaders.put("Book Authors", false);
        subHeaders.put("Professionals", false);
        subHeaders.put("Researchers", false);
        subHeaders.put("Institutions", false);
        subHeaders.put("Librarians", false);
        subHeaders.put("Corporations", false);
        subHeaders.put("Societies", false);
        subHeaders.put("Journal Editors", false);
        subHeaders.put("Government", false);

        //If there is "Who We Serve" sub-header, move mouse on it
        WebElement whoWeServe;
        try {
            whoWeServe = driver.findElement(By.xpath("//nav[@id='main-header-navbar']/ul/li/a[text()='WHO WE SERVE']/.."));

            //TODO: rewrite code with actions
            Locatable hoverItem = (Locatable) whoWeServe;
            Mouse mouse = ((HasInputDevices) driver).getMouse();
            mouse.mouseMove(hoverItem.getCoordinates());
        } catch (Exception e) {
            throw new Exception("Who We Serve sub-header not found");
        }

        List<WebElement> headerLinks = whoWeServe.findElements(By.xpath("./div[@id='Level1NavNode1']/ul/li/a"));

        //Check the first point
        isTheFirstPointTrue = (headerLinks.size() == 11);
        for (WebElement element : headerLinks) {
            String linkText = element.getText();
            if (subHeaders.containsKey(linkText))
                subHeaders.put(linkText, true);
        }

        //Check the second point
        boolean isThereAllHeaders = true;
        for (Boolean value : subHeaders.values())
            isThereAllHeaders = isThereAllHeaders && value;

        isTheSecondPointTrue = isThereAllHeaders;

        printResult();

        return isTheFirstPointTrue &&
                isTheSecondPointTrue;
    }

    private void printResult() {
        if (isTheFirstPointTrue)
            System.out.println("There are 11 items under resource sub-header");
        else
            System.out.println("There are more or less than 11 items under resource sub-header");

        for (String key : subHeaders.keySet())
            if (subHeaders.get(key))
                System.out.println("There is " + key + " title");
            else
                System.out.println(key + " title not found");

    }
}

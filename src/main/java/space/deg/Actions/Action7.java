package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import space.deg.Action;

import java.util.List;

/**
 * Action 7:
 * Enter "Java" and do not press search button
 *   - Area with related content is displayed right under the search header
 *   - On the "Suggestions" section, it has 4 words starting with "Java"
 *   - On the "Products" section, there are 5 titles and each title contain "Java" word
 *
 *   NOTE: The statement of the second point is "... it has 4 words ..." it means 4 or more words,
 *   when the statement of the third point is "... there are 5 titles ..." it means exactly 5 titles.
 *
 * Created by Degtyarev Ilya on 01.03.2019.
 * degtiv@yandex.ru
 */

public class Action7 implements Action {
    private boolean isTheFirstPointTrue = false;
    private boolean isTheSecondPointTrue = false;
    private boolean isTheThirdPointTrue = false;

    @Override
    public boolean act(WebDriver driver) throws Exception{

        //Try to find search input, enter "Java" in it
        WebElement mainNavigationSearchBox = null;
        WebElement inputField = null;
        try {
            mainNavigationSearchBox = driver.findElement(By.xpath("//div[@class='main-navigation-search']/div/form"));
            inputField = mainNavigationSearchBox.findElement(By.xpath("./div[@class='input-group']/input"));
            inputField.sendKeys("Java");
        } catch (Exception ex) {
            throw new Exception("Search input not found");
        }

        //TODO: rewrite with waiting until the page open
        try{Thread.sleep(5000);}catch (Exception ex){}

        //Try to find aside menu
        WebElement asideMenu = null;
        try {
            asideMenu = mainNavigationSearchBox.findElement(By.xpath("./aside"));
        } catch (Exception ex) {
            throw new Exception("Aside menu not found");
        }
        boolean isAsideMenuDisplayed = asideMenu.isDisplayed();

        //Check that aside menu is displayed right under the search header
        if (isAsideMenuDisplayed) {
            Point inputFieldPoint = inputField.getLocation();
            Point asideMenuPoint = asideMenu.getLocation();
            if ((inputFieldPoint.x == asideMenuPoint.x) &&
                    (inputFieldPoint.y + inputField.getSize().height == asideMenuPoint.y))
                isTheFirstPointTrue = true;
        }

        //Get elements of 'Suggestions' section
        List<WebElement> suggestionsItems = asideMenu.findElements(
                By.xpath("./section[@class[contains(.,'suggestions')]]/div/div"));

        int countOfJavaWords = 0;
        for(WebElement item : suggestionsItems) {
            String word = item.findElement(By.xpath("./a")).getText();
            if (word.startsWith("Java"))
                countOfJavaWords++;
        }

        isTheSecondPointTrue = (countOfJavaWords >= 4);

        //Get elements of 'Products' section
        List<WebElement> productsItems = asideMenu.findElements(
                By.xpath("./section[@class[contains(.,'products')]]/div/div"));

        countOfJavaWords = 0;
        for(WebElement item : productsItems) {
            String word = item.findElement(By.xpath("./a")).getText();
            if (word.toLowerCase().contains("java"))
                countOfJavaWords++;
        }

        isTheThirdPointTrue = (countOfJavaWords == 5);

        printResult();

        return isTheFirstPointTrue &&
                isTheSecondPointTrue &&
                isTheThirdPointTrue;
    }

    private void printResult() {
        if (isTheFirstPointTrue) {
            System.out.println("Area with related content is displayed right under the search header");
        } else {
            System.out.println("Area with related content is not displayed or displayed not under the search header");
        }
        
        if (isTheSecondPointTrue) {
            System.out.println("On the \"Suggestions\" section, it has 4 words starting with \"Java\"");
        } else {
            System.out.println("On the \"Suggestions\" section, it has less than 4 words starting with \"Java\"");
        }
        
        if (isTheThirdPointTrue) {
            System.out.println("On the \"Products\" section, there are 5 titles and each title contain \"Java\" word");
        } else {
            System.out.println("On the \"Products\" section, there are more or less than 5 titles and each title contain \"Java\" word");
        }
    }
}

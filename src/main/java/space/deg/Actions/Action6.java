package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import space.deg.Action;

/**
 * Action 6:
 * Do not enter anything in the search input and press search button
 *   - Nothing happens, home page is still displayed
 *
 * Created by Degtyarev Ilya on 28.02.2019.
 * degtiv@yandex.ru
 */

public class Action6 implements Action {
    private boolean nothingHappens = false;

    @Override
    public boolean act(WebDriver driver) throws Exception{
        boolean isAsideMenuHidden;
        boolean isStillOnHomePage;
        String currentURL = driver.getCurrentUrl();

        //If there is search input and the search button, click on the button;
        WebElement mainNavigationSearchBox = null;
        try {
            mainNavigationSearchBox = driver.findElement(By.xpath("//div[@class='main-navigation-search']/div/form"));
            WebElement button = mainNavigationSearchBox.findElement(By.xpath("./div[@class='input-group']/span/button"));
            button.click();
        } catch (Exception ex) {
            throw new Exception("Search button not found");
        }

        //If there is aside menu, check it is not displayed;
        //If there is no aside menu, probably xpath is wrong, it's an error.
        try {
            WebElement asideMenu = mainNavigationSearchBox.findElement(By.xpath("./aside"));
            isStillOnHomePage = driver.getCurrentUrl().equalsIgnoreCase(currentURL);
            isAsideMenuHidden = !asideMenu.isDisplayed();
        } catch (Exception ex) {
            throw new Exception("Aside menu not found");
        }

        nothingHappens = isAsideMenuHidden && isStillOnHomePage;

        printResult();

        return nothingHappens;
    }

    private void printResult() {
        if (nothingHappens)
            System.out.println("Nothing happens, home page is still displayed");
        else
            System.out.println("Something happens");
    }
}

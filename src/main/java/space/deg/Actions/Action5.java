package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import space.deg.Action;

/**
 * Action 5:
 * Click on the Wiley logo at the top menu (left side of the top menu)
 *   - Home page is opened
 *
 * Created by Degtyarev Ilya on 28.02.2019.
 * degtiv@yandex.ru
 */

public class Action5 implements Action {
    private boolean isHomePageOpened = false;

    @Override
    public boolean act(WebDriver driver) throws Exception{
        try {
            WebElement linkOfLogoElement = driver.findElement(By.xpath("//header//img[@title='Wiley']/.."));
            linkOfLogoElement.click();
        } catch (Exception e) {
            throw new Exception("Wiley logo not found");
        }

        //TODO: rewrite with waiting until the page open
        try{Thread.sleep(5000);}catch (Exception ex){}
        isHomePageOpened = driver.getCurrentUrl().equalsIgnoreCase("https://www.wiley.com/en-us");

        printResult();

        return isHomePageOpened;
    }

    private void printResult() {
        if (isHomePageOpened)
            System.out.println("Home page is opened");
        else
            System.out.println("Home page is not opened");
    }
}

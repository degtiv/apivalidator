package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import space.deg.Action;

/**
 * Action 1:
 * Open https://www.wiley.com/en-us
 * Check the following links are displayed in the top menu
 *   - Who We Serve
 *   - Subjects
 *   - About
 *
 * Created by Degtyarev Ilya on 27.02.2019.
 * degtiv@yandex.ru
 */

public class Action1 implements Action {
    private final String URL = "https://www.wiley.com/en-us";

    private boolean isWhoWeServeDisplayed = false;
    private boolean isSubjectsDisplayed = false;
    private boolean isAboutDisplayed = false;

    @Override
    public boolean act(WebDriver driver) throws Exception{
        driver.get(URL);

        //If there is modal window, press 'Yes' on it
        try {
            WebElement modalWindowButton = driver.findElement(By.xpath(
                    "//div[@class='modal-content']//div[@class='modal-footer']/button[text()='YES']"));
            modalWindowButton.click();
        } catch (Exception e) {
            System.out.println("Modal window not found");
        }

        //If there is element with id "main-header-navbar, check links
        try {
            WebElement mainHeaderNavbar = driver.findElement(By.id("main-header-navbar"));

            isWhoWeServeDisplayed = mainHeaderNavbar.findElement(By.linkText("WHO WE SERVE")).isDisplayed();
            isSubjectsDisplayed = mainHeaderNavbar.findElement(By.linkText("SUBJECTS")).isDisplayed();
            isAboutDisplayed = mainHeaderNavbar.findElement(By.linkText("ABOUT")).isDisplayed();
        } catch (Exception e) {
            throw new Exception("Top menu not found by id \'main-header-navbar\"");
        }

        printResult();

        return isWhoWeServeDisplayed &&
                isSubjectsDisplayed &&
                isAboutDisplayed;
    }

    private void printResult() {
        if (isWhoWeServeDisplayed)
            System.out.println("Who We Serve link is displayed");
        else
            System.out.println("Who We Serve link is not displayed");

        if (isSubjectsDisplayed)
            System.out.println("Subjects link is displayed");
        else
            System.out.println("Subjects link is not displayed");

        if (isAboutDisplayed)
            System.out.println("About link is displayed");
        else
            System.out.println("About link is not displayed");
    }
}

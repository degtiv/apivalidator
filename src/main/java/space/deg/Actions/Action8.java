package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import space.deg.Action;

import java.util.List;

/**
 * Action 8:
 * Click "SEARCH" button
 *   - Only titles containing "Java" are displayed
 *   - There are 10 titles
 *   - Each title has at least one "Add to Cart" button
 *
 * Created by Degtyarev Ilya on 01.03.2019.
 * degtiv@yandex.ru
 */

public class Action8 implements Action {
    private boolean isTheFirstPointTrue = false;
    private boolean isTheSecondPointTrue = false;
    private boolean isTheThirdPointTrue = false;

    @Override
    public boolean act(WebDriver driver) throws Exception{

        //Try to find search button and click on it
        try {
            WebElement mainNavigationSearchBox = driver.findElement(By.xpath("//div[@class='main-navigation-search']/div/form"));
            WebElement button = mainNavigationSearchBox.findElement(By.xpath("./div[@class='input-group']/span/button"));
            button.click();
        } catch (Exception ex) {
            throw new Exception("Search button not found");
        }

        //TODO: rewrite with waiting until the page open
        try{Thread.sleep(5000);}catch (Exception ex){}

        List<WebElement> items = driver.findElements(By.xpath("//div[@class='search-result-tabs-wrapper']/div/div/div/div[@class='products-list']/section"));

        isTheSecondPointTrue = (items.size() == 10);

        //Check all elements contains 'java' and has 'add to cart' button
        boolean allOfElementsContainsJava = true;
        boolean allOfElementsHasAddToCartBtn = true;
        for (WebElement item : items) {
            String title = item.findElement(By.xpath("./div[@class='product-content']/h3")).getText();
            if (!title.toLowerCase().contains("java"))
                allOfElementsContainsJava = false;

            List<WebElement> addToCartButtons = item.findElements(By.xpath(".//button[text()='Add to cart']"));
            if (addToCartButtons.size() == 0)
                allOfElementsHasAddToCartBtn = false;

            //System.out.println(title + " : " + addToCartButtons.size());
        }

        isTheFirstPointTrue = allOfElementsContainsJava;
        isTheThirdPointTrue = allOfElementsHasAddToCartBtn;

        printResult();

        return isTheFirstPointTrue &&
                isTheSecondPointTrue &&
                isTheThirdPointTrue;
    }

    private void printResult() {
        if (isTheFirstPointTrue) {
            System.out.println("Only titles containing \"Java\" are displayed");
        } else {
            System.out.println("At least one title not containing \"Java\" is displayed");
        }
        
        if (isTheSecondPointTrue) {
            System.out.println("There are 10 titles");
        } else {
            System.out.println("There are more or less than 10 titles");
        }
        
        if (isTheThirdPointTrue) {
            System.out.println("Each title has at least one \"Add to Cart\" button");
        } else {
            System.out.println("At least one title does not have \"Add to Cart\" button");
        }
    }
}

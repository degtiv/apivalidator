package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.interactions.Mouse;
import space.deg.Action;

import java.util.HashMap;
import java.util.List;

/**
 * Action 4:
 * Go to "Subjects" top menu, select "Education"
 *   - Check "Education" header is displayed
 *   - 13 items are displayed under "Subjects" on the left side of the screen and the texts are:
 *      - "Information & Library Science",
 *      - "Education & Public Policy",
 *      - "K-12 General",
 *      - "Higher Education General",
 *      - "Vocational Technology",
 *      - "Conflict Resolution & Meditation (School settings)",
 *      - "Curriculum Tools-General",
 *      - "Special Educational Needs",
 *      - "Theory of Education",
 *      - "Education Special Topics",
 *      - "Educational Research & Statistics",
 *      - "Literacy & Reading",
 *      - "Classroom Management"
 *
 * Created by Degtyarev Ilya on 28.02.2019.
 * degtiv@yandex.ru
 */

public class Action4 implements Action {
    private boolean isEducationDisplayed = false;
    private boolean isNumberOfItemsEquals13 = false;
    private boolean isThereAllHeaders;
    private HashMap<String, Boolean> subHeaders = new HashMap<>();

    @Override
    public boolean act(WebDriver driver) throws Exception{
        subHeaders.put("Information & Library Science", false);
        subHeaders.put("Education & Public Policy", false);
        subHeaders.put("K-12 General", false);
        subHeaders.put("Higher Education General", false);
        subHeaders.put("Vocational Technology", false);
        subHeaders.put("Conflict Resolution & Meditation (School settings)", false);
        subHeaders.put("Curriculum Tools-General", false);
        subHeaders.put("Special Educational Needs", false);
        subHeaders.put("Theory of Education", false);
        subHeaders.put("Education Special Topics", false);
        subHeaders.put("Educational Research & Statistics", false);
        subHeaders.put("Literacy & Reading", false);
        subHeaders.put("Classroom Management", false);

        WebElement subjectElement = null;
        Mouse mouse = ((HasInputDevices) driver).getMouse();

        //If there is "Subjects" sub-header, move mouse on it
        try {
            subjectElement = driver.findElement(By.xpath("//nav[@id='main-header-navbar']/ul/li/a[text()='SUBJECTS']/.."));

            //TODO: rewrite code with actions
            Locatable hoverSubject = (Locatable) subjectElement;

            mouse.mouseMove(hoverSubject.getCoordinates());
        } catch (Exception ex) {
            throw new Exception("\"Subjects\" sub-header not found");
        }

        //If there is "Education" item, check it is displayed and move mouse on it
        WebElement educationElement = null;
        try {
            educationElement = subjectElement.findElement(By.xpath("./div/ul/li/a[text()='Education']/.."));
            isEducationDisplayed = educationElement.isDisplayed();

            //TODO: rewrite code with actions
            Locatable hoverEducation = (Locatable) educationElement;
            mouse.mouseMove(hoverEducation.getCoordinates());
        } catch (Exception e) {
            throw new Exception("\"Education\" item not found");
        }

        //Get all sub-headers of 'Subjects' header
        List<WebElement> items = educationElement.findElements(By.xpath("./div/ul/ul/li/a"));
        isNumberOfItemsEquals13 = (items.size() == 13);

        //Set 'true' for displayed sub-headers
        for (WebElement item : items) {
            String linkText = item.getText();
            if (subHeaders.containsKey(linkText))
                subHeaders.put(linkText, true);
        }

        //Check that all sub-headers are displayed
        isThereAllHeaders = true;
        for (Boolean value : subHeaders.values())
            isThereAllHeaders = isThereAllHeaders && value;

        printResult();

        return isEducationDisplayed &&
                isNumberOfItemsEquals13 &&
                isThereAllHeaders;
    }

    private void printResult() {
        if (isEducationDisplayed)
            System.out.println("\"Education\" header is displayed");
        else
            System.out.println("\"Education\" header is not displayed");

        if (isNumberOfItemsEquals13)
            System.out.println("Number of items is 13");
        else
            System.out.println("Number of items is more or less than 13");

        for (String key : subHeaders.keySet())
            if (subHeaders.get(key))
                System.out.println("There is " + key + " item");
            else
                System.out.println(key + " item not found");
    }
}

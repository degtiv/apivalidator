package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import space.deg.Action;

import java.util.List;

/**
 * Action 3:
 * Click "Students" item
 *   - Check that https://www.wiley.com/en-us/students url is opened
 *   - Check that "Students" header is displayed
 *   - Check that "Learn More" links are present on the page and direct to www.wileyplus.com site
 *
 * Created by Degtyarev Ilya on 28.02.2019.
 * degtiv@yandex.ru
 */
public class Action3 implements Action {

    private boolean isRequiredUrlOpened = false;
    private boolean isStudentsHeaderDisplayed = false;
    private boolean isLearnMorePresent = false;
    private boolean isLinkDirectToWileyPlus = false;

    @Override
    public boolean act(WebDriver driver) throws Exception{
        WebElement studentsLink = null;
        //If there is "Students" link, click on it
        try {
            studentsLink = driver.findElement(By.xpath("//div[@id='Level1NavNode1']//a[text()='Students']"));
            studentsLink.click();
        } catch (Exception e) {
            throw new Exception("\"Students\" link not found");
        }

        //TODO: rewrite with waiting until the page open
        try{Thread.sleep(5000);}catch (Exception ex) {}

        isRequiredUrlOpened = driver.getCurrentUrl().
                equalsIgnoreCase("https://www.wiley.com/en-us/students");

        //If there is header, check title is equal "Students"
        try {
            isStudentsHeaderDisplayed = driver.findElement(
                    By.xpath("//div[@class='row']//div[@class='hero-banner collection-banner']//span[text()='Students']")).
                    isDisplayed();
        } catch (Exception ex) {
            throw new Exception("Header not found");
        }

        //Find all 'Learn More' links
        List<WebElement> learnMoreElements = driver.findElements(By.xpath("//span[text()[contains(.,'Learn More')]]/../.."));
        if (learnMoreElements.size() > 0)
            isLearnMorePresent = true;

        //Check that all 'Learn More' links direct to www.wileyplus.com site
        try {
            isLinkDirectToWileyPlus = true;
            for (WebElement link : learnMoreElements) {
                isLinkDirectToWileyPlus = isLinkDirectToWileyPlus &&
                        link.getAttribute("href").contains("www.wileyplus.com");
            }
        } catch (Exception ex) {
            isLearnMorePresent = false;
            System.out.println("Some of links does not have 'href' attribute");
        }

        printResult();

        return isRequiredUrlOpened &&
                isStudentsHeaderDisplayed &&
                isLearnMorePresent &&
                isLinkDirectToWileyPlus;
    }

    private void printResult() {
        if (isRequiredUrlOpened)
            System.out.println("https://www.wiley.com/en-us/students is opened");
        else
            System.out.println("https://www.wiley.com/en-us/students is not opened");

        if (isStudentsHeaderDisplayed)
            System.out.println("\"Students\" header is displayed");
        else
            System.out.println("\"Students\" header is not displayed");

        if (isLearnMorePresent)
            System.out.println("At least one \"Learn More\" link is present");
        else
            System.out.println("None \"Learn More\" link is present");

        if (isLinkDirectToWileyPlus)
            System.out.println("All \"Learn More\" links direct to www.wileyplus.com site");
        else
            System.out.println("One or more \"Learn More\" links do not direct to www.wileyplus.com site");
    }
}

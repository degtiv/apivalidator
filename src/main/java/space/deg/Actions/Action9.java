package space.deg.Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import space.deg.Action;

import java.util.HashSet;
import java.util.List;

/**
 * Action 9:
 * Enter "Java" in the search input at the top and press "SEARCH" button
 *   - Make sure there are same 10 titles shown (as in step 8)
 *
 * Created by Degtyarev Ilya on 01.03.2019.
 * degtiv@yandex.ru
 */

public class Action9 implements Action {
    private boolean isTheFirstPointTrue = false;

    @Override
    public boolean act(WebDriver driver) throws Exception{
        //Get items before press 'search' button
        List<WebElement> items = driver.findElements(By.xpath("//div[@class='search-result-tabs-wrapper']/div/div/div/div[@class='products-list']/section"));
        HashSet<String> titleNames = new HashSet<>();
        for (WebElement item : items) {
            String title = item.findElement(By.xpath("./div[@class='product-content']/h3")).getText();
            titleNames.add(title);
        }

        //Try to find search input and button
        try {
            WebElement mainNavigationSearchBox = driver.findElement(By.xpath("//div[@class='main-navigation-search']/div/form"));
            WebElement inputField = mainNavigationSearchBox.findElement(By.xpath("./div[@class='input-group']/input"));
            inputField.sendKeys("Java");
            WebElement button = mainNavigationSearchBox.findElement(By.xpath("./div[@class='input-group']/span/button"));
            button.click();
        } catch (Exception ex) {
            throw new Exception("Search input or button not found");
        }

        //TODO: rewrite with waiting until the page open
        try{Thread.sleep(5000);}catch (Exception ex){}

        //Get items after enter 'Java' and press 'search' button. And check items are the same
        boolean areThereSame10Titles = true;
        items = driver.findElements(By.xpath("//div[@class='search-result-tabs-wrapper']/div/div/div/div[@class='products-list']/section"));
        for (WebElement item : items) {
            String title = item.findElement(By.xpath("./div[@class='product-content']/h3")).getText();
            if (!titleNames.contains(title))
                areThereSame10Titles = false;
        }
        boolean isNumberOfItems10 = (items.size() == 10);

        isTheFirstPointTrue = areThereSame10Titles && isNumberOfItems10;

        printResult();

        return isTheFirstPointTrue;
    }

    private void printResult() {
        if (isTheFirstPointTrue) {
            System.out.println("There are same 10 titles shown (as in step 8)");
        } else {
            System.out.println("There is more or less than 10 titles. Or there are other titles");
        }
    }
}

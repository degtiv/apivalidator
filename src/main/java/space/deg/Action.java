package space.deg;

import org.openqa.selenium.WebDriver;

/**
 * Created by Degtyarev Ilya on 27.02.2019.
 * degtiv@yandex.ru
 */

public interface Action {
    boolean act(WebDriver driver) throws Exception;
}

package space.deg;

/**
 * Created by Degtyarev Ilya on 27.02.2019.
 * degtiv@yandex.ru
 */
public class Launcher {

    public static void main(String[] args) {
        SeleniumWebDriverChecker seleniumWebDriverChecker = new SeleniumWebDriverChecker();
        seleniumWebDriverChecker.act();

        DelayAndPngImageChecker delayAndPngImageChecker = new DelayAndPngImageChecker();
        delayAndPngImageChecker.act();
    }
}

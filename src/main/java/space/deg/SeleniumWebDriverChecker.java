package space.deg;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import space.deg.Actions.*;

/**
 * Created by Degtyarev Ilya on 27.02.2019.
 * degtiv@yandex.ru
 */

public class SeleniumWebDriverChecker  {
    List<Action> actions;
    WebDriver driver;

    public void act () {
        System.setProperty("webdriver.gecko.driver", "D:\\driver\\geckodriver.exe");

        actions = new ArrayList<>();
        actions.add(new Action1());
        actions.add(new Action2());
        actions.add(new Action3());
        actions.add(new Action4());
        actions.add(new Action5());
        actions.add(new Action6());
        actions.add(new Action7());
        actions.add(new Action8());
        actions.add(new Action9());

        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        runScenario();

        driver.quit();
    }

    private void runScenario() {
        int i = 1;
        for (Action action : actions) {
            System.out.println(i + ") Action " + i);
            try {
                if (action.act(driver)) {
                    Log.printSuccess();
                } else {
                    Log.printFailure();
                }
            } catch (Exception e) {
                Log.printException(e.toString());
            }
            i++;
        }
    }



}
